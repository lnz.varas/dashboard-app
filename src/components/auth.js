import React, { useEffect } from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { useHistory } from 'react-router-dom';
import { getCookie } from 'utils/cookies';

function Auth({ children }) {
  const userName = useStoreState(state => state.user.userName);
  const updateUserName = useStoreActions(actions => actions.user.updateUserName);
  const history = useHistory();

  useEffect(() => {
    // Check cookie if name is set
    // if not set redirect login
    const name = getCookie('name');
    if (name === '') {
      history.push("/");
    } else {
      // update userName in redux if cookie exists
      if (userName === '') {
        updateUserName(name)
      }
    }
  }, [history, userName, updateUserName])

  return (
    <>
      {children}
    </>
  );
}

export default Auth;
