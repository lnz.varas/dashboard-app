import React, { useState } from 'react';
import styled from 'styled-components';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Col, Row, Container, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { Link } from "react-router-dom";
import { deleteCookie } from 'utils/cookies';
import { useHistory } from 'react-router-dom';

const HeaderContainer = styled.div`
  min-height: 50px;
  background: #333;
`

function Header() {
  const [dropdownOpen, setOpen] = useState(false);
  const userName = useStoreState(state => state.user.userName);
  const updateUserName = useStoreActions(actions => actions.user.updateUserName);

  const history = useHistory();
  const toggle = () => setOpen(!dropdownOpen);

  const logOut = () => {
    updateUserName('');
    deleteCookie('name');
    history.push("/");
  }

  return (
    <HeaderContainer>
      <Container>
        <Row xs="2">
          <Col className="text-left text-white pt-1">
            <h3>Welcome back, { userName }</h3>
          </Col>
          <Col className="text-right pt-1">
            <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
              <DropdownToggle caret>
                Menu
                </DropdownToggle>
              <DropdownMenu>
                <DropdownItem>
                  <Link to="/dashboard">
                    Dashboard
                  </Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/bookmarks">
                    Favourites
                  </Link>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  <div onClick={logOut}>
                    Logout
                  </div>
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
          </Col>
        </Row>
      </Container>
    </HeaderContainer>
  )
}
export default Header;