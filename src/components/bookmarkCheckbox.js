import React from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Input } from 'reactstrap';

function BookmarkCheckbox(activityId) {
  const updateFavourite = useStoreActions(actions => actions.activity.updateFavourite);
  const favourites = useStoreState(state => state.activity.favourites);

  const saveFavourite = (a) => {
    updateFavourite(a.id)
  }

  return (
    <form>
      <Input
        type="checkbox"
        name="bookmark"
        value="0"
        checked={favourites.indexOf(activityId.id) !== -1 && 'checked'}
        onChange={(e) => saveFavourite(activityId)}
      />
    </form>
  )
}
export default BookmarkCheckbox;