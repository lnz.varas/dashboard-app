import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { StoreProvider } from 'easy-peasy';

import store from 'store';

import Login from 'pages/login';
import Dashboard from 'pages/dashboard';
import Bookmars from 'pages/bookmarks';

import Auth from 'components/auth';

function App({ data, localeItems, locale }) {

  return (
    <StoreProvider store={store({ data, localeItems, locale })}>
      <Router>
        <Route exact path="/">
          <Login />
        </Route>
        <Auth>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/bookmarks">
            <Bookmars />
          </Route>
        </Auth>
      </Router>
    </StoreProvider>
  );
}

export default App;
