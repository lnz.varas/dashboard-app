import { thunk, action } from 'easy-peasy';
import axios from 'axios';


export default {
  userName: '',
  updateUserName: action((state, payload) => {
    state.userName = payload;
  }),
}
