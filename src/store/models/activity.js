import { thunk, action } from 'easy-peasy';
import axios from 'axios';

import endpoints from 'config/endpoints';

export default {
  activities: [],
  filteredActivities: [],
  favourites: [],
  updateActivities: action((state, payload) => {
    state.activities = payload;
    state.filteredActivities = payload;
  }),
  filterActivities: action((state, payload) => {
    state.filteredActivities = state.activities.filter((a) => {
      return a.activity.toLowerCase().indexOf(payload.toLowerCase()) !== -1 
      || a.type.toLowerCase().indexOf(payload.toLowerCase()) !== -1
      }
    );
  }),
  updateFavourite: action((state, payload) => {
    const index = state.favourites.indexOf(payload)
    if (index !== -1) {
      state.favourites = state.favourites.filter((f) => f !== payload);
    } else {
      state.favourites = [...state.favourites, payload];
    }
  }),
  fetchActivities: thunk(
    async (actions) => {
      const { data } = await axios.get(endpoints.activities);
      const activities = data.slice(0, 10);
      actions.updateActivities(activities);
    },
  ),
}
