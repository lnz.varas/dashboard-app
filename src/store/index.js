import { createStore } from 'easy-peasy'

import user from './models/user';
import activity from './models/activity';

export default () =>
  createStore(
    {
      user,
      activity
    }
  )
