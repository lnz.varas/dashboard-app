export function isOnlyAlphanumeric (text) {
  return /^[a-zA-Z]+$/.test(text);
}