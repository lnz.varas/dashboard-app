
const cookieCheck = cVal => {
  return (typeof (cVal) !== 'undefined' && cVal !== '' );
}

export function setCookie(name, value, minutes) {
  if (!cookieCheck(value)) {
    return;
  }
  const date = new Date();
  date.setTime(date.getTime() + (minutes * 60 * 1000));
  document.cookie = name + "=" + value + "; expires=" + date.toGMTString();
}

export function getCookie(name) {
  const cookieName = name + "=";
  const allCookieArray = document.cookie.split(';');
  for (var i = 0; i < allCookieArray.length; i++) {
    const temp = allCookieArray[i].trim();
    if (temp.indexOf(cookieName) === 0)
      return temp.substring(cookieName.length, temp.length);
  }
  return "";
}

export function deleteCookie(name) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}