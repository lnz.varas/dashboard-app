import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useStoreState, useStoreActions } from 'easy-peasy';
import {
  Col, Container, Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, Row
} from 'reactstrap';
import Header from 'components/header.js';

const BookmarksPage = styled.div`
  height: 100vh;
  background: url('/assets/skulls.svg');
  .card {
    height: 100%;
  }
`

function Dashboard() {
  const fetchActivities = useStoreActions(actions => actions.activity.fetchActivities);
  const updateFavourite = useStoreActions(actions => actions.activity.updateFavourite);
  const activities = useStoreState(state => state.activity.activities);
  const favourites = useStoreState(state => state.activity.favourites);

  useEffect(() => {
    fetchActivities();
  }, [fetchActivities]);

  const removeActivity = (id) => {
    updateFavourite(id);
  }

  return (
    <BookmarksPage>
      <Header />
      <Container className="bg-white mt-4 py-5 content shadow-box">
        <Row>
          <Col>
            <h2 className="mb-5">Favourites activities</h2>
          </Col>
        </Row>
        <Row>
          {activities.map((a) => (
              <>
                {(favourites.indexOf(a.id) !== -1) && (
                  <Col xs="4" className="mb-4" key={a.id}>
                    <Card>
                    <CardImg 
                      top 
                      width="100%" 
                      src={`https://placeimg.com/318/180/tech}`} 
                      alt="Card image cap" 
                    />
                      <CardBody>
                        <CardTitle>
                          <b>{a.activity}</b>
                        </CardTitle>
                        <CardSubtitle className="pb-4">
                          <i>{a.type}</i>
                        </CardSubtitle>
                        <CardText>
                          Price:
                          <b> {a.price}</b>
                          <br />
                          Participants:
                          <b>{ a.participants}</b>
                        </CardText>
                        <Button onClick={() => removeActivity(a.id)}>Remove</Button>
                      </CardBody>
                    </Card>
                  </Col>
                )}
              </>
            )
          )}
          {(favourites.length === 0) && (
            <Col>
              <p className="mb-5">No activities bookmarked...</p>
            </Col>
          )}
        </Row>
      </Container>
    </BookmarksPage>
  );
}

export default Dashboard;
