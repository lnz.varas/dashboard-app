import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Button, FormGroup, Label, Input, Alert } from 'reactstrap';
import { useHistory } from 'react-router-dom';

import { isOnlyAlphanumeric } from 'utils/formValidator';
import { setCookie, getCookie } from 'utils/cookies';

const LoginContainer = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: url('/assets/skulls.svg');
`

const LoginBox = styled.div`
  width: 100%;
  max-width: 320px;
  flex: none;
  background: #fff;
  padding: 30px;
  box-shadow: 0 1px 1px 0 rgba(0,0,0,0.14), 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 3px 0 rgba(0,0,0,0.2);
  text-align: center;
`

function Login() {
  const [error, setError] = useState(false);
  const updateUserName = useStoreActions(actions => actions.user.updateUserName);
  const userName = useStoreState(state => state.user.userName);

  const history = useHistory();

  useEffect(() => {
    // If redux has already name set
    if (userName !== '') {
      history.push("/dashboard");
    }
    // Check cookie if name is set
    const name = getCookie('name');
    if (name) {
      history.push("/dashboard");
      updateUserName(name);
    }
  }, [history, updateUserName, userName])

  const submitForm = (e) => {
    e.preventDefault();
    const name = e.target.name.value.trim();
    if ( isOnlyAlphanumeric(name) ) {
      updateUserName(name);
      setCookie('name', name);
      history.push("/dashboard");
      setError(false)
    } else {
      setError(true)
    }
  }

  return (
    <LoginContainer>
      <LoginBox>
        <form onSubmit={submitForm}>
          <FormGroup>
            <Label for="exampleEmail"><h3>Name</h3></Label>
            {(error) && (
              <Alert color="danger">
                Only letters are allowed
              </Alert>
            )}
            <Input required type="text" name="name" id="exampleEmail" placeholder="Enter your name.." />
            <Button className="w-100 mt-4" color="primary">Enter</Button>
          </FormGroup>
        </form>
      </LoginBox>
    </LoginContainer>
  );
}

export default Login;
