import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Col, Container, Table, Row, Input, Button } from 'reactstrap';

import BookmarkCheckbox from 'components/bookmarkCheckbox.js';
import Header from 'components/header.js';

const DashboardContainer = styled.div`
  min-height: 100vh;
  background: url('/assets/skulls.svg');
  .reset-btn {
    min-width: 180px;
  }
`

function Dashboard() {
  const fetchActivities = useStoreActions(actions => actions.activity.fetchActivities);
  const filteredActivities = useStoreState(state => state.activity.filteredActivities);
  const filterActivities = useStoreActions(actions => actions.activity.filterActivities);
  
  useEffect(() => {
    fetchActivities();
  }, [fetchActivities]);

  const searchActivity = (e) => {
    const text = e.target.value;
    filterActivities(text)
  }

  const refresh = () => {
    fetchActivities();
  }

  return (
    <DashboardContainer>
      <Header />
      <Container className="bg-white mt-4 py-5 content shadow-box">
        <Col>
          <Row>
            <Col xs="6">
              <h2>Activities</h2>
            </Col>
            <Col xs="6" className="text-right d-flex">
              <Button className="reset-btn mb-2 mr-4" onClick={refresh}>Refresh table</Button>
              <Input 
                type="text" 
                name="search" 
                id="exampleEmail" 
                onKeyUp={searchActivity} 
                onKeyDown={searchActivity} 
                placeholder="Search activity by name or type..." 
              />
            </Col>
          </Row>
          <Table>
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Type</th>
                <th>Participants</th>
                <th>Price</th>
                <th>Bookmark</th>
              </tr>
            </thead>
            <tbody>
              {filteredActivities.map((a) => 
                <tr key={a.id}>
                  <th scope="row">{a.id}</th>
                  <td>{a.activity}</td>
                  <td>{a.type}</td>
                  <td>{a.participants}</td>
                  <td>{a.price}</td>
                  <td className="text-center">
                    <BookmarkCheckbox id={a.id} />
                  </td>
                </tr>
              )}
            </tbody>
          </Table>
        </Col>
      </Container>
    </DashboardContainer>
  );
}

export default Dashboard;
